<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');
Route::resource('user', 'UserController');

Route::resource('anggota', 'AnggotaController');

Route::resource('buku', 'BukuController');
// Route::get('/format_buku', 'BukuController@format');
// Route::post('/import_buku', 'BukuController@import');

Route::resource('transaksi', 'TransaksiController');


