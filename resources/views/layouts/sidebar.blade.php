<ul class="nav">
          @if(Auth::user()->level == 'admin')
          <li class="nav-item {{ setActive(['anggota*', 'buku*', 'user*']) }}">
            <div class="collapse {{ setShow(['anggota*', 'buku*', 'user*']) }}" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link {{ setActive(['anggota*']) }}" href="{{route('anggota.index')}}">Data Anggota</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ setActive(['buku*']) }}" href="{{route('buku.index')}}">Data Buku</a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link {{ setActive(['user*']) }}" href="{{route('user.index')}}">Data User</a>
                </li>
              </ul>
            </div>
          </li>
          @endif
          <li class="nav-item {{ setActive(['transaksi*']) }}">
            <a class="nav-link" href="{{route('transaksi.index')}}">
              <i class="menu-icon mdi mdi-backup-restore"></i>
              <span class="menu-title">Transaksi</span>
            </a>
          </li>
          
         
        </ul>